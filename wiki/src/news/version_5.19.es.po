# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2023-10-31 09:58+0100\n"
"PO-Revision-Date: 2024-04-23 22:42+0000\n"
"Last-Translator: victor dargallo <victordargallo@disroot.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.3\n"

#. type: Plain text
#, markdown-text, no-wrap
msgid "[[!meta title=\"Tails 5.19\"]]\n"
msgstr "[[!meta title=\"Tails 5.19\"]]\n"

#. type: Plain text
#, markdown-text, no-wrap
msgid "[[!meta date=\"Tue, 31 Oct 2023 12:34:56 +0000\"]]\n"
msgstr "[[!meta date=\"Tue, 31 Oct 2023 12:34:56 +0000\"]]\n"

#. type: Plain text
#, markdown-text, no-wrap
msgid "[[!pagetemplate template=\"news.tmpl\"]]\n"
msgstr "[[!pagetemplate template=\"news.tmpl\"]]\n"

#. type: Plain text
#, markdown-text, no-wrap
msgid "[[!tag announce]]\n"
msgstr "[[!tag announce]]\n"

#. type: Plain text
#, markdown-text, no-wrap
msgid "<h1 id=\"features\">New features</h1>\n"
msgstr "<h1 id=\"features\">Nuevas funcionalidades</h1>\n"

#. type: Plain text
#, markdown-text, no-wrap
msgid "<h2>Closing a Tor circuit from <i>Onion Circuits</i></h2>\n"
msgstr ""

#. type: Plain text
#, markdown-text
msgid ""
"You can now close a given Tor circuit from the *Onion Circuits* "
"interface. This can help replace a particularly slow Tor circuit or "
"troubleshoot issues on the Tor network."
msgstr ""

#. type: Plain text
#, markdown-text, no-wrap
msgid "[[!img close_circuit.png link=\"no\" alt=\"\"]]\n"
msgstr ""

#. type: Plain text
#, markdown-text
msgid "To close a Tor circuit:"
msgstr "Para cerrar un circuito Tor:"

#. type: Bullet: '1. '
#, markdown-text
msgid "[[Connect to the Tor network.|doc/anonymous_internet/tor]]"
msgstr "[[Conéctate a la red Tor.|doc/anonymous_internet/tor]]"

#. type: Bullet: '1. '
#, markdown-text
msgid ""
"Choose [[!img lib/symbolic/tor-connected.png alt=\"Tor status menu\" "
"link=\"no\" class=\"symbolic\"]]&nbsp;▸ **Open Onion Circuits** in the top "
"navigation bar."
msgstr ""

#. type: Bullet: '1. '
#, markdown-text
msgid ""
"Right-click (on Mac, click with two fingers) on the circuit that you want to "
"close."
msgstr ""

#. type: Bullet: '1. '
#, markdown-text
msgid "Choose **Close this circuit** in the shortcut menu."
msgstr ""

#. type: Plain text
#, markdown-text, no-wrap
msgid ""
"   When you close a circuit that is being used by an application, your\n"
"   application gets disconnected from this destination service.\n"
msgstr ""

#. type: Plain text
#, markdown-text, no-wrap
msgid ""
"   For example, when you close a circuit while <i>Tor Browser</i> is\n"
"   downloading a file, the download fails.\n"
msgstr ""

#. type: Bullet: '1. '
#, markdown-text
msgid ""
"If you connect to the same destination server again, Tor uses a different "
"circuit to replace the circuit that you closed."
msgstr ""

#. type: Plain text
#, markdown-text, no-wrap
msgid ""
"   For example, if you download the same file again, Tor uses a new "
"circuit.\n"
msgstr ""

#. type: Plain text
#, markdown-text, no-wrap
msgid "<h2>Addition of <span class=\"code\">sq-keyring-linter</span></h2>\n"
msgstr ""

#. type: Plain text
#, markdown-text
msgid ""
"At the request of people who use [SecureDrop](https://securedrop.org/) to "
"provide secure whistleblowing platforms across the world, we added the "
"[`sq-keyring-linter`](https://tracker.debian.org/pkg/rust-sequoia-keyring-linter)  "
"package. `sq-keyring-linter` improves the cryptographic parameters of PGP "
"keys stored in their airgapped machines."
msgstr ""

#. type: Plain text
#, markdown-text, no-wrap
msgid "<h1 id=\"changes\">Changes and updates</h1>\n"
msgstr "<h1 id=\"changes\">Cambios y actualizaciones</h1>\n"

#. type: Plain text
#, markdown-text
msgid ""
"- Update *Tor Browser* to "
"[13.0.1](https://blog.torproject.org/new-release-tor-browser-1301)."
msgstr ""

#. type: Plain text
#, markdown-text
msgid "- Update the *Tor* client to 0.4.8.7."
msgstr ""

#. type: Plain text
#, markdown-text
msgid ""
"- Update *Thunderbird* to "
"[115.4.1](https://www.thunderbird.net/en-US/thunderbird/115.4.1/releasenotes/)."
msgstr ""

#. type: Plain text
#, markdown-text
msgid "- Update the *Linux* kernel to 6.1.55."
msgstr ""

#. type: Plain text
#, markdown-text, no-wrap
msgid "<h1 id=\"fixes\">Fixed problems</h1>\n"
msgstr "<h1 id=\"fixes\">Cambios y actualizaciones</h1>\n"

#. type: Plain text
#, markdown-text
msgid ""
"For more details, read our [[!tails_gitweb debian/changelog "
"desc=\"changelog\"]]."
msgstr ""
"Para más detalles, lee nuestro [[!tails_gitweb debian/changelog desc="
"\"registro de cambios\"]]."

#. type: Plain text
#, markdown-text, no-wrap
msgid "<h1 id=\"issues\">Known issues</h1>\n"
msgstr "<h1 id=\"issues\">Problemas conocidos</h1>\n"

#. type: Plain text
#, markdown-text
msgid "None specific to this release."
msgstr "Nada concreto para esta versión."

#. type: Plain text
#, markdown-text
msgid "See the list of [[long-standing issues|support/known_issues]]."
msgstr ""

#. type: Plain text
#, markdown-text, no-wrap
msgid "<h1 id=\"get\">Get Tails 5.19</h1>\n"
msgstr ""

#. type: Title ##
#, markdown-text, no-wrap
msgid "To upgrade your Tails USB stick and keep your Persistent Storage"
msgstr "Para actualizar Tails y mantener tu Almacenamiento Persistente"

#. type: Plain text
#, markdown-text
msgid "- Automatic upgrades are available from Tails 5.0 or later to 5.19."
msgstr ""

#. type: Plain text
#, markdown-text, no-wrap
msgid ""
"  You can [[reduce the size of the download|doc/upgrade#reduce]] of future\n"
"  automatic upgrades by doing a manual upgrade to the latest version.\n"
msgstr ""
"  Puedes [[reducir el tamaño de la descarga|doc/upgrade#reduce]] de futuras\n"
"  actualizaciones automáticas haciendo una actualización manual a la última "
"versión.\n"

#. type: Bullet: '- '
#, markdown-text
msgid ""
"If you cannot do an automatic upgrade or if Tails fails to start after an "
"automatic upgrade, please try to do a [[manual "
"upgrade|doc/upgrade/#manual]]."
msgstr ""
"Si no puedes hacer una actualización automática, o si Tails falla al iniciar "
"después de una actualización automática, intenta hacer una [[actualización "
"manual|doc/upgrade#manual]]."

#. type: Title ##
#, markdown-text, no-wrap
msgid "To install Tails on a new USB stick"
msgstr "Para instalar Tails en una nueva memoria USB"

#. type: Plain text
#, markdown-text
msgid "Follow our installation instructions:"
msgstr "Sigue nuestras instrucciones de instalación:"

#. type: Bullet: '  - '
#, markdown-text
msgid "[[Install from Windows|install/windows]]"
msgstr "[[Instalar desde Windows|install/windows]]"

#. type: Bullet: '  - '
#, markdown-text
msgid "[[Install from macOS|install/mac]]"
msgstr "[[Instalar desde macOS|install/mac]]"

#. type: Bullet: '  - '
#, markdown-text
msgid "[[Install from Linux|install/linux]]"
msgstr "[[Instalar desde Linux|install/linux]]"

#. type: Bullet: '  - '
#, markdown-text
msgid ""
"[[Install from Debian or Ubuntu using the command line and "
"GnuPG|install/expert]]"
msgstr ""
"[[Instalar desde Debian o Ubuntu usando la linea de comandos y GnuPG|install/"
"expert]]"

#. type: Plain text
#, markdown-text, no-wrap
msgid ""
"<div class=\"caution\"><p>The Persistent Storage on the USB stick will be "
"lost if\n"
"you install instead of upgrading.</p></div>\n"
msgstr ""
"<div class=\"caution\"><p>El Almacenamiento Persistente en la memoria USB se "
"perderá si\n"
"instalas en vez de actualizar.</p></div>\n"

#. type: Title ##
#, markdown-text, no-wrap
msgid "To download only"
msgstr "Para sólo descargar"

#. type: Plain text
#, markdown-text
msgid ""
"If you don't need installation or upgrade instructions, you can download "
"Tails 5.19 directly:"
msgstr ""

#. type: Bullet: '  - '
#, markdown-text
msgid "[[For USB sticks (USB image)|install/download]]"
msgstr "[[Para memorias USB (imagen USB)|install/download]]"

#. type: Bullet: '  - '
#, markdown-text
msgid "[[For DVDs and virtual machines (ISO image)|install/download-iso]]"
msgstr "[[Para DVD y máquinas virtuales (imagen ISO)|install/download-iso]]"
